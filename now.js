module.exports = function NowResponseBuilder() {
  return function() {
    return {
        now: new Date().toISOString()
    }
  }
}

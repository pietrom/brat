const assert = require('assert')
const {describe, it} = require("mocha")
const InfoResponseBuilder = require('../info')
const builder = new InfoResponseBuilder('test', 'xxx')
describe('InfoResponseBuilder', function () {
    describe('when invoked', function () {
        it('should return provided envName', function () {
            assert.equal(builder().environment, 'test')
        })

        it('should return provided version', function () {
            assert.equal(builder().version, 'xxx')
        })
    })
})

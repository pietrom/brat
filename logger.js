const methods = ['trace', 'debug', 'info', 'warn', 'error', 'log']
module.exports = function Logger() {
  for(let method of methods) {
    this[method] = function() {
      console[method](...arguments)
    }
  }

  this.info('Logger initialized', methods.join())
}

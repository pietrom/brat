const http = require('http')
const os = require("os")
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')
const { createHttpTerminator } = require('http-terminator')

const port = process.env.PORT || 1919
const envName = process.env.ENVIRONMENT_NAME
const { buildTag, builtAt } = require('./meta')
const hostName = os.hostname()

function randomInt(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1))
}

async function sleep(delay) {
    return new Promise((resolve) => {
        setTimeout(resolve, delay)
    })
}

const infoHandler = require('./info')(envName, buildTag, hostName, builtAt)
const nowHandler = require('./now')()
const promClient = require('prom-client')
promClient.collectDefaultMetrics()
const nowCounter = new promClient.Counter({
  name: 'now_calls',
  help: 'Count calls to /now',
});
const infoCounter = new promClient.Counter({
  name: 'info_calls',
  help: 'Count calls to /meta/info',
});
const reqDuration = new promClient.Summary({
  name: 'request_duration',
  help: 'Duration of HTTP requests',
  labelNames: ['url']
});
const register = promClient.register

const Logger = require('./logger')
const logger = new Logger()

let minDelay = 250
let maxDelay = 750

const handlers = {
    '/now': async (req, res, done) => {
        nowCounter.inc()
        done(200, JSON.stringify(nowHandler()), {'Content-Type': 'application/json'})
    },
    '/meta/info': async (req, res, done) => {
        infoCounter.inc()
        done(200, JSON.stringify(infoHandler()), {'Content-Type': 'application/json'})
    },
    '/metrics': async (req, res, done) => {
      const metrics = await register.metrics()
      done(200, metrics, {'Content-Type': 'text/plain'})
    },
    '/probes/liveness': async (req, res, done) => {
        done(204)
    },
    '/probes/readiness': async (req, res, done) => {
        await sleep(randomInt(100, 400))
        done(204)
    },
    '/data': async (req, res, done) => {
        await sleep(randomInt(minDelay, maxDelay))
        done(200, JSON.stringify([
            { text: 'one', number: 1 },
            { text: 'two', number: 2 },
            { text: 'three', number: 3 },
            { text: 'four', number: 4 },
            { text: 'five', number: 5 },
            { text: 'six', number: 6 }
        ]), {'Content-Type': 'application/json'})
    },
    '/fs-data': async (req, res, done) => {
      await sleep(randomInt(minDelay, maxDelay))
      fs.readFile('/fs-data/test-data', 'utf8', function (err, data) {
        if (err) {
          done(500, err, { 'Content-Type': 'text/html', 'X-Brat-Host': hostName })
        }
        done(200, data, { 'Content-Type': 'text/html', 'X-Brat-Host': hostName })
      })
    },
    '/config': async (req, res, done) => {
        let body = ""
        req.on('readable', function() {
            const chunk = req.read()
            if(chunk) {
                body += chunk
            }
        })
        req.on('end', function() {
            const payload = JSON.parse(body)
            const left = payload.minDelay || minDelay
            const right = payload.maxDelay || maxDelay
            if(left <= right) {
                minDelay = left
                maxDelay = right
            }
            done(204)
        })
    }
}

const defaultHandler = (req, res, done) => {
    done(200, `Hello, K8S World!!! [${envName}: ${buildTag}]`, {'Content-Type': 'text/plain'})
}

function doWithDebug(req, res, f) {
  const reqId = uuidv4()
  logger.debug(`${reqId} start ${new Date().toISOString()} on ${hostName}`, req.method, req.url)
  f(req, res, (status, payload, headers) => {
    logger.debug(`${reqId} end  ${new Date().toISOString()}`, status, payload, headers)
  })
}

const server = http.createServer(async function (req, res) {
    doWithDebug(req, res, async (request, response, done) => {
      const handler = handlers[request.url] || defaultHandler
      const end = reqDuration.startTimer()
      await handler(request, response, function(status, payload, headers) {
        response.writeHead(status, headers || {})
        if(payload) {
          response.write(payload)
        }
        end({ url : request.url })
        response.end()
        done(status, payload, headers)
      })
    })
}).listen(port)

const httpTerminator = createHttpTerminator({
  server,
})

function now() {
  return new Date().toISOString()
}

function closed() {
  logger.info('Server closed at', now())
}

async function close(signal) {
  logger.info(`${signal} signal received at`, now());
  await httpTerminator.terminate()
}

process.on('SIGTERM', () => {
    close('SIGTERM').then(() => logger.log('Server stopped at', now()))
})

process.on('SIGINT', () => {
    close('SIGINT').then(() => logger.log('Server stopped at', now()))
})

logger.info('HTTP server started on port', port, 'at', now())

FROM node:16.13.2-slim
ARG BUILD_TAG="dev"
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --only=production
COPY . .
RUN sed -i "s/buildTag: \"local-dev\"/buildTag: \"${BUILD_TAG}\"/" meta.js
RUN sed -i "s/builtAt: \"unknown\"/builtAt: \"$(date --iso-8601=ns)\"/" meta.js
EXPOSE 1919
ENTRYPOINT ["node", "index.js"]

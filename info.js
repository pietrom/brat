const unknown = 'unknown'

module.exports = function InfoResponseBuilder(envName, buildTag, hostName, builtAt) {
  return function() {
    return {
      environment: envName || unknown,
      hostName: hostName || unknown,
      version: buildTag || unknown,
      builtAt: builtAt || unknown
    }
  }
}
